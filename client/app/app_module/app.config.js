var app = angular.module(MAIN_MODULE);
app.config(['$routeProvider',
    function config($routeProvider) {
      $routeProvider.
        when('/live-stream', {
          template: '<live-stream></live-stream>'
        }).
        when('/start-live', {
          template: '<start-live></start-live>'
        }).
        otherwise('/live-stream');
    }
  ]);