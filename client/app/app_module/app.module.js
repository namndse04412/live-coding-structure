angular.module(MAIN_MODULE, [
  'ngRoute',
  'ngResource',
  'liveStream',
  'startLive'
]);

angular.element(function() {
      angular.bootstrap(document, [MAIN_MODULE]);
});